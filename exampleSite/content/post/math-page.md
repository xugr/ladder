---
title: Math Page
math: true
date: 2020-05-05
---

\\[ \int_{a}^{b} x^2 dx \\]

An inline equation: \\( \int_{a}^{b} x^2 dx \\).

{{< katex display >}}
\int_{a}^{b} x^2 dx
{{< /katex >}}

{{< mermaid class="text-center">}}
sequenceDiagram
    Alice->>Bob: Hello Bob, how are you?
    alt is sick
        Bob->>Alice: Not so good :(
    else is well
        Bob->>Alice: Feeling fresh like a daisy
    end
    opt Extra response
        Bob->>Alice: Thanks for asking
    end
{{< /mermaid >}}