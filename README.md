# Ladder

## Overview

Modified version of [Angel's Ladder](https://github.com/tanksuzuki/angels-ladder), a simple blog theme for Hugo.

* Simple and clean design
* Responsive design
* Pagination
* Tagging
* Disqus
* Source code highlighting
* Google Analytics

## Demo

https://blog.xugr.me
